<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:forEach items="${requestScope.comments}" var="comment"
	varStatus="status">

	<blockquote class="clear-float">
		<p>
			${comment.content}
		</p>
		<small class="place-right"> 
			${comment.id}楼
			<time title="发布时间">
			 	<fmt:formatDate value="${comment.date}" pattern="YYYY.MM.dd HH:mm"/>
			</time>
		</small>
	</blockquote>
	<hr class="thin">
</c:forEach>
<c:if test="${requestScope.page != null}">
	<div class="pagination">
		<c:if test="${!requestScope.page.first}">
			<span class="item" data-page="${1}">首页</span> 
			<span class="item" data-page="${requestScope.page.currentPage-1}">上页</span> 
		</c:if>	
		<c:forEach items="${requestScope.page.rows}" var="p">
			<span class="item <c:if test="${requestScope.page.currentPage == p}">current</c:if> " data-page="${p}">${p}</span> 
		</c:forEach>
		<c:if test="${!requestScope.page.last}">
			<span class="item " data-page="${requestScope.page.currentPage+1}">下页</span> 
			<span class="item" data-page="${requestScope.page.count}">尾页</span> 
		</c:if>
		<script>
			$(".pagination .item").click(function(){
				var $page = $(this).data("page");
				$("#comments-container").load($page + "/comments");
			})
		</script>
	</div>
</c:if>