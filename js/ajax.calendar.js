var $desktop;
var $calendar;
var $comments;
var $map;
var $phone;

$(function() {

	$.get("ajax/calendar.html", function(data) {
		setupWindow(data, "calendar.html");
		$calendar = data;
		prevLoad();
		$("#calendar").addClass("bg-dark border-bottom");
	});

	

	var prevLoad = function() {
		$.get("ajax/desktop.html", function(data) {
			$desktop = data;
		});

		$.get("ajax/comments.html", function(data) {
			$comments = data;
		});

		$.get("ajax/map.html", function(data) {
			$map = data;
		});

		$.get("ajax/phone.html", function(data) {
			$phone = data;
		});
	}

})
