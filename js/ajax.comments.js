var $desktop;
var $calendar;
var $comments;
var $map;
var $phone;

$(function() {
	$.get("ajax/comments.html", function(data) {
		setupWindow(data, "comments.html");
		$comments = data;
		prevLoad();

		$("#comments").addClass("bg-dark border-bottom");

	});

	var prevLoad = function() {
		$.get("ajax/desktop.html", function(data) {
			$desktop = data;
		});

		$.get("ajax/map.html", function(data) {
			$map = data;
		});

		$.get("ajax/calendar.html", function(data) {
			$calendar = data;
		});

		$.get("ajax/phone.html", function(data) {
			$phone = data;
		});
	}

})
