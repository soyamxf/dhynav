var $desktop;
var $calendar;
var $comments;
var $map;
var $phone;

$(function() {

	$.get("ajax/desktop.html", function(data) {
		setupWindow(data, "desktop.html");
		$desktop = data;
		prevLoad();
	});


	var prevLoad = function() {
		$.get("ajax/map.html", function(data) {
			$map = data;
		});

		$.get("ajax/comments.html", function(data) {
			$comments = data;
		});

		$.get("ajax/calendar.html", function(data) {
			$calendar = data;
		});

		$.get("ajax/phone.html", function(data) {
			$phone = data;
		});
	}

})
