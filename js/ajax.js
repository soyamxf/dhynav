var $desktop;
var $calendar;
var $comments;
var $map;
var $phone;

$(function() {

	$.get("ajax/desktop.html", function(data) {
		$desktop = data;
	});

	$.get("ajax/calendar.html", function(data) {
		$calendar = data;
	});

	$.get("ajax/comments.html", function(data) {
		$comments = data;
	});

	$.get("ajax/map.html", function(data) {
		$map = data;
	});

	$.get("ajax/phone.html", function(data) {
		$phone = data;
	});

	$("#calendar").click(function() {
		if ($calendar != null) {
			setupWindow($calendar)
		} else {
			$.get("ajax/calendar.html", function(data) {
				$calendar = data;
				setupWindow(data)
			});
		}
	});
	$("#comments").click(function() {
		if ($calendar != null) {
			setupWindow($comments)
		} else {
			$.get("ajax/comments.html", function(data) {
				$comments = data;
				setupWindow(data)
			});
		}
	});
	$("#map").click(function() {
		if ($map != null) {
			setupWindow($map)
		} else {
			$.get("ajax/map.html", function(data) {
				$map = data;
				setupWindow(data)
			});
		}
	});
	$("#phone").click(function() {
		if ($phone != null) {
			setupWindow($phone)
		} else {
			$.get("ajax/phone.html", function(data) {
				$phone = data;
				setupWindow(data)
			});
		}
	});
	var setupWindow = function(data) {
		$(".toolbar button").removeClass("border-bottom")
				.removeClass("bg-dark");
		$(".container").html(data);
	}
})