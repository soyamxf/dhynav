var $desktop;
var $calendar;
var $comments;
var $map;
var $phone;

$(function() {

	$.get("ajax/phone.html", function(data) {
		setupWindow(data, "phone.html");
		$phone = data;
		prevLoad();
		$("#phone").addClass("bg-dark border-bottom");

	});


	var prevLoad = function() {
		$.get("ajax/desktop.html", function(data) {
			$desktop = data;
		});

		$.get("ajax/comments.html", function(data) {
			$comments = data;
		});

		$.get("ajax/calendar.html", function(data) {
			$calendar = data;
		});

		$.get("ajax/map.html", function(data) {
			$map = data;
		});
	}

	$("#phone").click(function() {
		if ($phone != null) {
			setupWindow($phone)
		} else {
			$.get("ajax/phone.html", function(data) {
				$phone = data;
				setupWindow(data)
			});
		}
		$(this).addClass("bg-dark border-bottom");
		return false;
	});
})
