var up = false;

$(function() {
	
  $("#thumbup").mouseover(function(event) {
    $("#thumbup span").addClass("mif-ani-bounce");
  }).mouseout(function(event) {
    $("#thumbup span").removeClass("mif-ani-bounce");
  }).click(function(event) {
    if (up) {
      $.Notify({
        caption: '您已经点过赞了',
        content: '谢谢您的支持，谢谢哦亲。',
        type: 'warning'
      });
    } else {
      $.post("thumbsup.do",function(){
    	  up = true;
          $.Notify({
            caption: '点赞成功',
            content: '谢谢您的支持，小站将会继续努力！',
            type: 'success'
          });
      })
    }

  });
  

  $("#share").mouseover(function(event) {
	$("#share span").addClass("mif-ani-ripple");
  }).mouseout(function(event) {
	$("#share span").removeClass("mif-ani-ripple");
  });
  
 
  var agree=getCookie("agree");
  if(agree){
    $("#against").attr('disabled', 'disabled');
    $("#agree").attr('disabled', 'disabled');
  }

  $("#agree").click(function(event) {
    $(this).attr('disabled', 'disabled');
    $("#against").attr('disabled', 'disabled');
    $.post("agree.do");
    setCookie("agree", "agree", 3600000);
  });
  
  $("#against").click(function(event) {
    $(this).attr('disabled', 'disabled');
    $.post("disagree.do");
    setCookie("agree", "disagree", 3600000);
  });
 
})



function AddFavorite(sURL, sTitle) {
  sURL = encodeURI(sURL);
  try {
    window.external.addFavorite(sURL, sTitle);
    $.Notify({
      caption: '收藏成功',
      content: '谢谢您的支持，小站将会继续努力！',
      type: 'success'
    });
  } catch (e) {
    try {
      window.sidebar.addPanel(sTitle, sURL, "");
      $.Notify({
        caption: '收藏成功',
        content: '谢谢您的支持，小站将会继续努力！',
        type: 'success'
      });
    } catch (e) {
      metroDialog.toggle('#ctrld');
    }

  }

}
