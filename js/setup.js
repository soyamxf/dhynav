$(function() {
	$("#calendar").click(function() {
		loading();
		if ($calendar != null) {
			setupWindow($calendar, "calendar.html")
		} else {
			$.get("ajax/calendar.html", function(data) {
				$calendar = data;
				setupWindow(data, "calendar.html")
			});
		}
		$(this).addClass("bg-dark border-bottom");
		return false;
	});

	$("#comments").click(function() {
		loading();
		if ($calendar != null) {
			setupWindow($comments, "comments.html")
		} else {
			$.get("ajax/comments.html", function(data) {
				$comments = data;
				setupWindow(data, "comments.html")
			});
		}
		$(this).addClass("bg-dark border-bottom");
		return false;
	});
	$("#map").click(function() {
		loading();

		if ($map != null) {
			setupWindow($map, "map.html")
		} else {
			$.get("ajax/map.html", function(data) {
				$map = data;
				setupWindow(data, "map.html")
			});
		}
		$(this).addClass("bg-dark border-bottom");
		return false;
	});

	$("#phone").click(function() {
		loading();

		if ($phone != null) {
			setupWindow($phone, "phone.html")
		} else {
			$.get("ajax/phone.html", function(data) {
				$phone = data;
				setupWindow(data, "phone.html")
			});
		}
		$(this).addClass("bg-dark border-bottom");
		return false;
	});
})

var setupWindow = function(data, url) {
	$(".toolbar button").removeClass("border-bottom").removeClass("bg-dark");
	$(".container").html(data);
	history.replaceState({
		container : $('body').html(),
		url : url
	}, null, url)

}

function closeWindow() {
	if ($desktop != null) {
		setupWindow($desktop, "desktop.html")
	} else {
		$.get("ajax/desktop.html", function(data) {
			$calendar = data;
			setupWindow(data, "desktop.html")
		});
	}
	return false;
}

function loading() {
	var data = '<div style="margin:200px auto; width: 200px;">'
			+ '<span class="icon mif-spinner3 mif-3x mif-ani-pulse fg-white"></span>'
			+ '<span class="fg-white header padding10 text-shadow">加载中...</span></div>';
	$(".container").html(data);
}
